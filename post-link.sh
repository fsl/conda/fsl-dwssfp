if [ -e ${FSLDIR}/share/fsl/sbin/createFSLWrapper ]; then
    ${FSLDIR}/share/fsl/sbin/createFSLWrapper dtifit_ssfp dwssfp_bedpostx dwssfp_bedpostx_postproc.sh dwssfp_bedpostx_preproc.sh dwssfp_bedpostx_single_slice.sh dwssfp_dtifit dwssfp_dtifit_postproc.sh dwssfp_dtifit_preproc.sh dwssfp_dtifit_single_slice.sh xfibres_ssfp
fi
